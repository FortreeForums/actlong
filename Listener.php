<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Activity/Longevity Meters ("ActLong").
 *
 *  ActLong is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ActLong is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ActLong.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ActLong;

use XF;
use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Manager;
use XF\Mvc\Entity\Structure;
use XF\Searcher\User;
use XF\Template\Templater;

class Listener
{
    public static function templaterMacroPreRender(Templater $templater, &$type, &$template, 
    &$name, array &$arguments, array &$globalVars)
    {
        if(!empty($arguments['group']) && $arguments['group']->group_id == 'ap_actlong')
        {
            $template = 'ap_actlong_option_macros';
        }
    }

    public static function userEntityStructure(Manager $em, Structure &$structure)
    {
        $structure->columns['ap_actlong_activity_meter'] = [
            'type'    => Entity::UINT, 
            'default' => NULL
        ];
    }
	
    public static function userSearcherOrders(User $userSearcher, array &$sortOrders)
    {
        $sortOrders['ap_actlong_activity_meter'] = XF::phrase('ap_actlong_activity');
    }
}
