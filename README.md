[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Inspired by [WetWired's vBulletin mod](https://www.vbulletin.org/forum/showthread.php?t=93831)

This addon adds an "activity" and a "longevity" meter to users postbits and profiles.

A cron job runs at a configured timeframe (daily, weekly, monthly) which resets the activity meters to 0.

### Features

- Supports [OzzModz] Post Comments
- Supports [UW] Forum Comments System
- Supports [AP] Minimum Characters to Post Count
- Admin options
    - Disable activity meter
    - Disable longevity meter
    - Required posts to fill the activity meter to 100%
    - Show meters in postbit
    - Show meters in profile
    - Reset activity meters daily, weekly, or monthly
    - Exclude specified nodes from counting towards activity meter
    - Count post comments towards activity meter if a supported addon is installed
    - Set a "start date" for your forum, instead of using the registration date of User ID #1
    - "View activity meter" permission
    - "View longevity meter" permission
- Style properties
    - Progress bar background
    - Progress bar color
    - Progress bar height
