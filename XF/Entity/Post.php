<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Activity/Longevity Meters ("ActLong").
 *
 *  ActLong is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ActLong is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ActLong.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ActLong\XF\Entity;

class Post extends XFCP_Post
{
    /**
     * Check if [OzzModz] Post Comments is installed,
     * and return early if so since those comments are still
     * considered posts.
     * 
     * Check if [AP] Minimum Characters for Post Count is
     * installed, and only increase the activity meter
     * if the message length passes the character check.
     *
     * @param integer $amount
     */
    protected function adjustUserMessageCountIfNeeded($amount)
    {
        if(!$this->user_id || !$this->User)
        {
            return parent::adjustUserMessageCountIfNeeded($amount);
        }
        
        $app = $this->app();
        $addons  = $app->container('addon.cache');
        $options = $app->options();

        if(array_key_exists('ThemeHouse/PostComments', $addons)
        && $this->thpostcomments_depth >= 1
        && !$options->apActLongCountCommentsTowardsActivity
        || in_array($this->Thread->node_id, $options->apActLongActivityExcludedNodes))
        {
            return parent::adjustUserMessageCountIfNeeded($amount);
        }

        if(array_key_exists('apathy/MinChars', $addons))
        {
            /** @var \apathy\MinChar\Repository\Counter $repo */
            $repo = $this->repository('apathy\MinChars:Counter');

            if($repo->messageLengthPassesCheck($this->message))
            {
                $this->User->fastUpdate('ap_actlong_activity_meter', max(0, $this->User->ap_actlong_activity_meter + $amount));
            }

            return parent::adjustUserMessageCountIfNeeded($amount);
        }
		
        $this->User->fastUpdate('ap_actlong_activity_meter', max(0, $this->User->ap_actlong_activity_meter + $amount));

        return parent::adjustUserMessageCountIfNeeded($amount);
    }
}
