<?php

/**
 *  ▄▄▄·  ▄▄▄· ▄▄▄· ▄▄▄▄▄ ▄ .▄ ▄· ▄▌
 * ▐█ ▀█ ▐█ ▄█▐█ ▀█ •██  ██▪▐█▐█▪██▌
 * ▄█▀▀█  ██▀·▄█▀▀█  ▐█.▪██▀▐█▐█▌▐█▪
 * ▐█ ▪▐▌▐█▪·•▐█ ▪▐▌ ▐█▌·██▌▐▀ ▐█▀·.
 *  ▀  ▀ .▀    ▀  ▀  ▀▀▀ ▀▀▀ ·  ▀ •
 *  <https://fortreeforums.xyz/>
 *  Licensed under GPL-3.0-or-later 2021
 *
 *  This file is part of [AP] Activity/Longevity Meters ("ActLong").
 *
 *  ActLong is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  ActLong is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ActLong.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace apathy\ActLong\Install;

use XF\Db\Schema\Alter;

trait upgrade1020070
{
    public function upgrade1020070Step1()
    {
        $optionIds = [
            'ap_actlong_show_postbit'      => 'apActLongShowMetersPostbit',
            'ap_actlong_show_profile'      => 'apActLongShowMetersProfile',
            'ap_actlong_show_tooltip'      => 'apActLongShowMetersTooltip',
            'ap_actlong_disable_activity'  => 'apActLongActivityMeterDisabled',
            'ap_actlong_maxposts'          => 'apActLongActivityMaxPosts',
            'ap_actlong_timeframe'         => 'apActLongActivityTimeframe',
            'ap_actlong_exclude_nodes'     => 'apActLongActivityExcludedNodes',
            'ap_actlong_disable_longevity' => 'apActLongLongevityMeterDisabled'
        ];

		$db = $this->db();
		$db->beginTransaction();

		foreach($optionIds AS $from => $to)
		{
			$db->update('xf_option', ['option_id' => $to], 'option_id = ?', $from);
		}

		$db->commit();
    }

    public function upgrade1020070Step2()
    {
        $this->schemaManager()->alterTable('xf_user', function(Alter $table)
        {
            $table->renameColumn('ap_al_activity_meter', 'ap_actlong_activity_meter');
        });
    }
}
